#!/bin/bash
# pre-commit.sh is for check some error before commit
# 1. stash unstashed changes
# STASH_NAME="pre-commit-$(date +%s)"
# git stash save --quiet --keep-index --include-untracked $STASH_NAME

# Test prospective commit
# 2. check errors
# 2.1 check compile error
make pick_best_seed
make
make debug
make clean

# 2.2 check code format
# 2.2.1 trailing whitespace
# find and fix trailing whitespace in your commits. 
# Bypass it with the --no-verify option to git-commit
# credits: https://github.com/imoldman/config/blob/master/pre-commit.git.sh

#if [[ "x`git status -s | grep '^[A|D|M]'`" == "x" ]]; then
  ## empty commit
  #exit 0
#fi

# detect platform
platform="win"
uname_result=`uname`
if [[ "$uname_result" == "Linux" ]]; then
  platform="linux"
elif [[ "$uname_result" == "Darwin" ]]; then
  platform="mac"
fi

# change IFS to ignore filename's space in |for|
IFS="
"
# autoremove trailing whitespace
for line in `git diff --check --cached | sed '/^[+-]/d'` ; do
  # get file name
  if [[ "$platform" == "mac" ]]; then
    file="`echo $line | sed -E 's/:[0-9]+: .*//'`"
  else
    file="`echo $line | sed -r 's/:[0-9]+: .*//'`"
  fi  
  # display tips
  echo -e "auto remove trailing whitespace in \033[31m$file\033[0m!"
  # since $file in working directory isn't always equal to $file in index, so we backup it
  mv -f "$file" "${file}.save"
  # discard changes in working directory
  git checkout -- "$file"
  # remove trailing whitespace
  if [[ "$platform" == "win" ]]; then
    # in windows, `sed -i` adds ready-only attribute to $file(I don't kown why), so we use temp file instead
    sed 's/[[:space:]]*$//' "$file" > "${file}.bak"
    mv -f "${file}.bak" "$file"
  elif [[ "$platform" == "mac" ]]; then
    sed -i "" 's/[[:space:]]*$//' "$file"
  else
    sed -i 's/[[:space:]]*$//' "$file"
  fi  
  git add "$file"
  # restore the $file
  sed 's/[[:space:]]*$//' "${file}.save" > "$file"
  rm "${file}.save"
done

# Now we can commit
exit

STASHES=$(git stash list)
if [[ $STASHES == *"$STASH_NAME" ]]; then
  git stash pop --quiet
fi



